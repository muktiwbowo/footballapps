package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderNext
import com.muktiwibowo.footballapps.model.ModelNext

class AdapterNext(val listNext: List<ModelNext>): RecyclerView.Adapter<HolderNext>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderNext {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_next, parent, false)
        return HolderNext(view)
    }

    override fun getItemCount(): Int = listNext.size

    override fun onBindViewHolder(holderNext: HolderNext, position: Int) {
        holderNext.bindNext(listNext[position])
    }
}