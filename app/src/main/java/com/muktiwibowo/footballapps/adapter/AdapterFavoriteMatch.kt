package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderFavoriteMatch
import com.muktiwibowo.footballapps.model.ModelFavorite

class AdapterFavoriteMatch(val listFavoriteMatch: List<ModelFavorite>): RecyclerView.Adapter<HolderFavoriteMatch>() {
    private lateinit var mFavoriteListener: FavoriteListener

    interface FavoriteListener{
        fun onFavoriteDeleted(position: Int)
    }

    fun setFavoriteLongListener(favoriteListener: FavoriteListener){
        mFavoriteListener    = favoriteListener
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderFavoriteMatch {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_favorite_match, parent, false)
        val holderFavoriteMatch = HolderFavoriteMatch(view)
        holderFavoriteMatch.setFavoriteListener(mFavoriteListener)
        return holderFavoriteMatch
    }

    override fun getItemCount(): Int = listFavoriteMatch.size

    override fun onBindViewHolder(holderFavoriteMatch: HolderFavoriteMatch, position: Int) {
        holderFavoriteMatch.bindFavorite(listFavoriteMatch[position])
    }
}