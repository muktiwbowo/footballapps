package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderTeam
import com.muktiwibowo.footballapps.model.ModelTeam

class AdapterTeam(val listTeam: List<ModelTeam>) : RecyclerView.Adapter<HolderTeam>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderTeam {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_team, parent, false)
        return HolderTeam(view)
    }

    override fun getItemCount(): Int = listTeam.size

    override fun onBindViewHolder(holderTeam: HolderTeam, position: Int) {
        holderTeam.bindTeam(listTeam[position])
    }
}