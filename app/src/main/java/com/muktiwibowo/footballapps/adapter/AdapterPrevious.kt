package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderPrevious
import com.muktiwibowo.footballapps.model.ModelPrevious

class AdapterPrevious(val listPrevious: List<ModelPrevious>): RecyclerView.Adapter<HolderPrevious>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderPrevious {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_previous, parent, false)
        return HolderPrevious(view)
    }

    override fun getItemCount(): Int = listPrevious.size

    override fun onBindViewHolder(holderPrevious: HolderPrevious, position: Int) {
        holderPrevious.bindMatch(listPrevious[position])
    }
}