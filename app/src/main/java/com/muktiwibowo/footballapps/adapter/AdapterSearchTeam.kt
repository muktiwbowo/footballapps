package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderSearchTeams
import com.muktiwibowo.footballapps.model.ModelSearchTeam

class AdapterSearchTeam(val listTeam: List<ModelSearchTeam>): RecyclerView.Adapter<HolderSearchTeams>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderSearchTeams {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_search_team, parent, false)
        return HolderSearchTeams(view)
    }

    override fun getItemCount(): Int = listTeam.size
    override fun onBindViewHolder(holderSearchTeams: HolderSearchTeams, position: Int) {
        holderSearchTeams.bindSearchTeam(listTeam[position])
    }
}