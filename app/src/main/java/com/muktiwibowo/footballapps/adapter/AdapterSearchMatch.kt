package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderSearchMatch
import com.muktiwibowo.footballapps.model.ModelSearchEvent

class AdapterSearchMatch(val listMatch: List<ModelSearchEvent>): RecyclerView.Adapter<HolderSearchMatch>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderSearchMatch {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_search_match, parent, false)
        return HolderSearchMatch(view)
    }

    override fun getItemCount(): Int = listMatch.size

    override fun onBindViewHolder(holderSearchMatch: HolderSearchMatch, position: Int) {
        holderSearchMatch.bindSearchMatch(listMatch[position])
    }
}