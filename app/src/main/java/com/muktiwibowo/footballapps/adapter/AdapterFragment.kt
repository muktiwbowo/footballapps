package com.muktiwibowo.footballapps.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class AdapterFragment(fm: FragmentManager?): FragmentPagerAdapter(fm) {
    private         var listFragment: ArrayList<Fragment>    = arrayListOf()
    private         var listTitle   : ArrayList<String>      = arrayListOf()

    override fun getItem(position: Int): Fragment = listFragment[position]

    override fun getCount(): Int = listTitle.size

    fun addFragment(fragment: Fragment, title: String?){
        listFragment.add(fragment)
        title?.let { listTitle.add(it) }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return listTitle[position]
    }
}