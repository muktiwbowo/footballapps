package com.muktiwibowo.footballapps.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.activity.ActivityContent
import com.muktiwibowo.footballapps.model.ModelFootball
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_football.view.*

class AdapterFootball(val context: Context, val listFootballs: List<ModelFootball>): BaseAdapter() {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val football = this.listFootballs[position]
        val view = LayoutInflater.from(context).inflate(R.layout.holder_football, parent, false)
        view.holder_football_name.text = football.footballName
        Glide.with(context).load(football.footballImage).into(view.holder_football_image)
        view.setOnClickListener {
            val intent = Intent(context, ActivityContent::class.java)
            intent.putExtra(UtilConstant.FOOTBALL_INTENT_ID_LEAGUE, football)
            context.startActivity(intent)
        }
        return view
    }

    override fun getItem(position: Int): Any = listFootballs[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = listFootballs.size
}