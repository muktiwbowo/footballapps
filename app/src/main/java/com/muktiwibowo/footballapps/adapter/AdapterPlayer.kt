package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderPlayer
import com.muktiwibowo.footballapps.model.ModelPlayer

class AdapterPlayer(val listPlayer: List<ModelPlayer>): RecyclerView.Adapter<HolderPlayer>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderPlayer {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_player, parent, false)
        return HolderPlayer(view)
    }

    override fun getItemCount(): Int = listPlayer.size

    override fun onBindViewHolder(holderPlayer: HolderPlayer, position: Int) {
        holderPlayer.bindPlayer(listPlayer[position])
    }
}