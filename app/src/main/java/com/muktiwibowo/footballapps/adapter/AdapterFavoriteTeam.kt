package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderFavoriteTeam
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam

class AdapterFavoriteTeam(val listFavoriteTeam: List<ModelFavoriteTeam>): RecyclerView.Adapter<HolderFavoriteTeam>() {
    private lateinit var mFavoriteListener: FavoriteListener

    interface FavoriteListener{
        fun onFavoriteDeleted(position: Int)
    }

    fun setFavoriteLongListener(favoriteListener: FavoriteListener){
        mFavoriteListener    = favoriteListener
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderFavoriteTeam {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_favorite_team, parent, false)
        val holderFavoriteTeam = HolderFavoriteTeam(view)
        holderFavoriteTeam.setFavoriteListener(mFavoriteListener)
        return holderFavoriteTeam
    }

    override fun getItemCount(): Int = listFavoriteTeam.size

    override fun onBindViewHolder(holderFavoriteTeam: HolderFavoriteTeam, position: Int) {
        holderFavoriteTeam.bindFavoriteTeam(listFavoriteTeam[position])
    }
}