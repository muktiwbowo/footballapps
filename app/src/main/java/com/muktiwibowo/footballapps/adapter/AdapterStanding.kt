package com.muktiwibowo.footballapps.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.holder.HolderStanding
import com.muktiwibowo.footballapps.model.ModelStanding

class AdapterStanding(val listStanding: List<ModelStanding>): RecyclerView.Adapter<HolderStanding>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderStanding {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_standing, parent, false)
        return HolderStanding(view)
    }

    override fun getItemCount(): Int = listStanding.size
    override fun onBindViewHolder(holderStanding: HolderStanding, position: Int) {
        holderStanding.bindStanding(listStanding[position])
    }
}