package com.muktiwibowo.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelFavoriteTeam (
        @SerializedName("id_")
        var idFavorite: Int,
        @SerializedName("team_id")
        var teamId: String?,
        @SerializedName("team_year")
        var teamName: String?,
        @SerializedName("team_stadium")
        var teamYear: String?,
        @SerializedName("team_country")
        var teamCountry: String?,
        @SerializedName("team_name")
        var teamStadium: String?,
        @SerializedName("team_badge")
        var teamBadge: String?
): Parcelable {
        companion object {
                const val FAVORITE_TEAM: String         = "favorite_team"
                const val ID: String                    = "id_"
                const val TEAM_ID: String               = "team_id"
                const val TEAM_NAME: String             = "team_name"
                const val TEAM_YEAR: String             = "team_year"
                const val TEAM_COUNTRY: String          = "team_country"
                const val TEAM_STADIUM: String          = "team_stadium"
                const val TEAM_BADGE: String            = "team_badge"
        }
}