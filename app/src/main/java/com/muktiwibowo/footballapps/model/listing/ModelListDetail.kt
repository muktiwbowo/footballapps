package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelDetail
import com.muktiwibowo.footballapps.model.ModelNext
import com.muktiwibowo.footballapps.model.ModelPrevious
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListDetail(
    @SerializedName("events")
    val listDetail: List<ModelDetail>
): Parcelable