package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelLeague
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListLeague (
    @SerializedName("leagues")
    val listLeagues: List<ModelLeague>
): Parcelable