package com.muktiwibowo.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelFavorite (
        @SerializedName("id_")
        var idFavorite: Int,
        @SerializedName("id_event")
        var idEvent: String,
        @SerializedName("date_event")
        var dateEvent: String?,
        @SerializedName("home_name")
        var homeTeam: String?,
        @SerializedName("home_score")
        var homeScore: String?,
        @SerializedName("away_name")
        var awayTeam: String?,
        @SerializedName("away_score")
        var awayScore: String?
): Parcelable {
        companion object {
                const val TABLE_FAVORITE: String        = "favorite_match"
                const val ID: String                    = "id_"
                const val ID_EVENT: String              = "id_event"
                const val HOME_NAME: String             = "home_name"
                const val HOME_SCORE: String            = "home_score"
                const val DATE_EVENT: String            = "date_event"
                const val AWAY_SCORE: String            = "away_score"
                const val AWAY_NAME: String             = "away_name"
        }
}