package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelPlayer
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListPlayer(
    @SerializedName("player")
    val listPlayers: List<ModelPlayer>
): Parcelable