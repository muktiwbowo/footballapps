package com.muktiwibowo.footballapps.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelFootball(
    var footballId: String?,
    var footballName: String?,
    var footballImage: Int?
): Parcelable