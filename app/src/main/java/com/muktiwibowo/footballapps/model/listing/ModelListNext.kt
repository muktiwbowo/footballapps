package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelNext
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListNext(
    @SerializedName("events")
    val listNexts: List<ModelNext>
): Parcelable