package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelSearchEvent
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListSearchEvent (
    @SerializedName("event")
    val listSearchEvents: List<ModelSearchEvent>
): Parcelable