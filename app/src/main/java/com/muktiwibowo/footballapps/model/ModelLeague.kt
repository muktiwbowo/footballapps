package com.muktiwibowo.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelLeague(
    @SerializedName("strDescriptionES")
    var strDescriptionES: String?,
    @SerializedName("dateFirstEvent")
    var dateFirstEvent: String?,
    @SerializedName("intFormedYear")
    var intFormedYear: String?,
    @SerializedName("strBanner")
    var strBanner: String?,
    @SerializedName("strSport")
    var strSport: String?,
    @SerializedName("strDescriptionIT")
    var strDescriptionIT: String?,
    @SerializedName("strDescriptionCN")
    var strDescriptionCN: String?,
    @SerializedName("strDescriptionEN")
    var strDescriptionEN: String?,
    @SerializedName("strWebsite")
    var strWebsite: String?,
    @SerializedName("strYoutube")
    var strYoutube: String?,
    @SerializedName("strDescriptionIL")
    var strDescriptionIL: String?,
    @SerializedName("idCup")
    var idCup: String?,
    @SerializedName("strComplete")
    var strComplete: String?,
    @SerializedName("strLocked")
    var strLocked: String?,
    @SerializedName("idLeague")
    var idLeague: String?,
    @SerializedName("idSoccerXML")
    var idSoccerXML: String?,
    @SerializedName("strTrophy")
    var strTrophy: String?,
    @SerializedName("strBadge")
    var strBadge: String?,
    @SerializedName("strTwitter")
    var strTwitter: String?,
    @SerializedName("strDescriptionHU")
    var strDescriptionHU: String?,
    @SerializedName("strGender")
    var strGender: String?,
    @SerializedName("strLeagueAlternate")
    var strLeagueAlternate: String?,
    @SerializedName("strDescriptionSE")
    var strDescriptionSE: String?,
    @SerializedName("strNaming")
    var strNaming: String?,
    @SerializedName("strDivision")
    var strDivision: String?,
    @SerializedName("strDescriptionJP")
    var strDescriptionJP: String?,
    @SerializedName("strFanart1")
    var strFanart1: String?,
    @SerializedName("strDescriptionFR")
    var strDescriptionFR: String?,
    @SerializedName("strFanart2")
    var strFanart2: String?,
    @SerializedName("strFanart3")
    var strFanart3: String?,
    @SerializedName("strFacebook")
    var strFacebook: String?,
    @SerializedName("strFanart4")
    var strFanart4: String?,
    @SerializedName("strCountry")
    var strCountry: String?,
    @SerializedName("strDescriptionNL")
    var strDescriptionNL: String?,
    @SerializedName("strRSS")
    var strRSS: String?,
    @SerializedName("strDescriptionRU")
    var strDescriptionRU: String?,
    @SerializedName("strDescriptionPT")
    var strDescriptionPT: String?,
    @SerializedName("strLogo")
    var strLogo: String?,
    @SerializedName("strDescriptionDE")
    var strDescriptionDE: String?,
    @SerializedName("strDescriptionNO")
    var strDescriptionNO: String?,
    @SerializedName("strLeague")
    var strLeague: String?,
    @SerializedName("strPoster")
    var strPoster: String?,
    @SerializedName("strDescriptionPL")
    var strDescriptionPL: String?
): Parcelable