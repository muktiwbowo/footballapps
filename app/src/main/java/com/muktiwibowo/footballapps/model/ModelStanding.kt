package com.muktiwibowo.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelStanding(
    @SerializedName("loss")
    var loss: String?,
    @SerializedName("total")
    var total: String?,
    @SerializedName("goalsfor")
    var goalsfor: String?,
    @SerializedName("goalsagainst")
    var goalsagainst: String?,
    @SerializedName("teamid")
    var teamid: String?,
    @SerializedName("goalsdifference")
    var goalsdifference: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("draw")
    var draw: String?,
    @SerializedName("played")
    var played: String?,
    @SerializedName("win")
    var win: String?
) : Parcelable