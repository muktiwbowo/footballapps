package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelDetailTeam
import com.muktiwibowo.footballapps.model.ModelTeam
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListDetailTeam(
    @SerializedName("teams")
    val listDetailTeams: List<ModelDetailTeam>
) : Parcelable