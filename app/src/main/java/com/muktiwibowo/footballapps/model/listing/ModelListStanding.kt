package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelStanding
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListStanding(
    @SerializedName("table")
    val listStandings: List<ModelStanding>
): Parcelable