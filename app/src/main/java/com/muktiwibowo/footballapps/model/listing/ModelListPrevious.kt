package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelPrevious
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListPrevious(
    @SerializedName("events")
    val listPrevious: List<ModelPrevious>
): Parcelable