package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelSearchTeam
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListSearchTeam(
    @SerializedName("teams")
    val listTeams: List<ModelSearchTeam>
) : Parcelable