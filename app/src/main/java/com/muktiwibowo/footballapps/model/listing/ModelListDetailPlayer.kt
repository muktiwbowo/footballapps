package com.muktiwibowo.footballapps.model.listing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.muktiwibowo.footballapps.model.ModelDetailPlayer
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelListDetailPlayer (
    @SerializedName("players")
    val listDetailPlayers: List<ModelDetailPlayer>
): Parcelable