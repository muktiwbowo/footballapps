package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterTeam
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelTeam
import com.muktiwibowo.footballapps.presenter.PresenterTeam
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewTeam
import kotlinx.android.synthetic.main.fragment_team.view.*

class FragmentTeam : Fragment(), ViewTeam {

    private             var mListTeam: MutableList<ModelTeam> = mutableListOf()
    private lateinit    var mAdapterTeam: AdapterTeam
    private lateinit    var mPresenterTeam: PresenterTeam
    private lateinit    var mLeagueId: String

    fun newInstance(leagueId: String?): FragmentTeam{
        val fragmentTeam        = FragmentTeam()
        val bundle              = Bundle()
        bundle.putString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE, leagueId)
        fragmentTeam.arguments  = bundle
        return fragmentTeam
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLeagueId = arguments?.getString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_team, container, false)
        initView(view)
        return view
    }

    override fun showTeam(listTeam: Collection<ModelTeam>) {
        mListTeam.clear()
        mListTeam.addAll(listTeam)
        mAdapterTeam.notifyDataSetChanged()
    }

    private fun initView(view: View) {
        view.team_recycler_view.layoutManager       = LinearLayoutManager(context)
        mAdapterTeam                                = AdapterTeam(mListTeam)
        view.team_recycler_view.adapter             = mAdapterTeam
        mPresenterTeam                              = PresenterTeam(this, ControllerApi())
        mPresenterTeam.loadTeam(mLeagueId)
    }


}
