package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterPlayer
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelPlayer
import com.muktiwibowo.footballapps.presenter.PresenterPlayer
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewPlayer
import kotlinx.android.synthetic.main.fragment_player.view.*

class FragmentPlayer : Fragment(), ViewPlayer {

    private             var mListPlayer: MutableList<ModelPlayer> = mutableListOf()
    private lateinit    var mAdapterPlayer: AdapterPlayer
    private lateinit    var mPresenterPlayer: PresenterPlayer
    private lateinit    var mIdTeam: String

    fun newInstance(idTeam: String?): FragmentPlayer{
        val fragmentPlayer  = FragmentPlayer()
        val bundle          = Bundle()
        bundle.putString(UtilConstant.FOOTBALL_ARGUMENT_ID_TEAM, idTeam)
        fragmentPlayer.arguments = bundle
        return fragmentPlayer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIdTeam = arguments?.getString(UtilConstant.FOOTBALL_ARGUMENT_ID_TEAM).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_player, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        view.playe_recycler_view.layoutManager  = LinearLayoutManager(context)
        mAdapterPlayer                          = AdapterPlayer(mListPlayer)
        view.playe_recycler_view.adapter        = mAdapterPlayer
        mPresenterPlayer                        = PresenterPlayer(this, ControllerApi())
        mPresenterPlayer.loadPlayer(mIdTeam)
    }

    override fun showPlayer(listPlayer: Collection<ModelPlayer>) {
        mListPlayer.clear()
        mListPlayer.addAll(listPlayer)
        mAdapterPlayer.notifyDataSetChanged()
    }
}
