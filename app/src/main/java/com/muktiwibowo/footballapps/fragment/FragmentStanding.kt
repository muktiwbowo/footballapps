package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterStanding
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelStanding
import com.muktiwibowo.footballapps.presenter.PresenterStanding
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewStanding
import kotlinx.android.synthetic.main.fragment_standings.*
import kotlinx.android.synthetic.main.fragment_standings.view.*

class FragmentStanding : Fragment(), ViewStanding {

    private             var mListStanding: MutableList<ModelStanding> = mutableListOf()
    private lateinit    var mAdapterStanding: AdapterStanding
    private lateinit    var mPresenterStanding: PresenterStanding
    private lateinit    var mLeagueId: String

    fun newInstance(leagueId: String?): FragmentStanding{
        val fragmentStanding = FragmentStanding()
        val bundle = Bundle()
        bundle.putString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE, leagueId)
        fragmentStanding.arguments = bundle
        return fragmentStanding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLeagueId = arguments?.getString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_standings, container, false)
        initView(view)
        return view
    }

    override fun showStanding(listStanding: Collection<ModelStanding>) {
        mListStanding.clear()
        mListStanding.addAll(listStanding)
        mAdapterStanding.notifyDataSetChanged()
    }

    private fun initView(view: View){
        view.standing_recycler_view.layoutManager   = LinearLayoutManager(context)
        mAdapterStanding                            = AdapterStanding(mListStanding)
        view.standing_recycler_view.adapter         = mAdapterStanding
        mPresenterStanding                          = PresenterStanding(this, ControllerApi())
        mPresenterStanding.loadStanding(mLeagueId)
    }


}
