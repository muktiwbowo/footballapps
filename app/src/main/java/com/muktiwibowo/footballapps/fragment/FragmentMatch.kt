package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterNext
import com.muktiwibowo.footballapps.adapter.AdapterPrevious
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelFootball
import com.muktiwibowo.footballapps.model.ModelLeague
import com.muktiwibowo.footballapps.model.ModelPrevious
import com.muktiwibowo.footballapps.model.ModelNext
import com.muktiwibowo.footballapps.presenter.PresenterMatch
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewMatch
import kotlinx.android.synthetic.main.fragment_match.*
import kotlinx.android.synthetic.main.fragment_match.view.*


class FragmentMatch : Fragment(), ViewMatch {

    private             var mListPrevious: MutableList<ModelPrevious> = mutableListOf()
    private             var mListNext: MutableList<ModelNext> = mutableListOf()
    private             var mListLeague: MutableList<ModelLeague> = mutableListOf()
    private lateinit    var mAdapterPrevious: AdapterPrevious
    private lateinit    var mAdapterNext: AdapterNext
    private lateinit    var mPresenterMatch: PresenterMatch
    private lateinit    var mLeagueId: String

    fun newInstance(leagueId: String?): FragmentMatch{
        val fragmentMatch   = FragmentMatch()
        val bundle          = Bundle()
        bundle.putString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE, leagueId)
        fragmentMatch.arguments = bundle
        return fragmentMatch
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLeagueId = arguments?.getString(UtilConstant.FOOTBALL_ARGUMENT_ID_LEAGUE).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_match, container, false)
        initView(view)
        return view
    }

    override fun showPrevious(listPrevious: Collection<ModelPrevious>) {
        mListPrevious.clear()
        mListPrevious.addAll(listPrevious)
        mAdapterPrevious.notifyDataSetChanged()
    }

    override fun showNext(listNext: Collection<ModelNext>) {
        mListNext.clear()
        mListNext.addAll(listNext)
        mAdapterNext.notifyDataSetChanged()
    }

    override fun showLeague(listLeague: Collection<ModelLeague>) {
        mListLeague.clear()
        mListLeague.addAll(listLeague)
        populateLeague(mListLeague)
    }

    private fun initView(view: View){
        view.match_previous_recycler_view.layoutManager     = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        view.match_next_recycler_view.layoutManager         = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        mAdapterPrevious                                    = AdapterPrevious(mListPrevious)
        mAdapterNext                                        = AdapterNext(mListNext)
        view.match_previous_recycler_view.adapter           = mAdapterPrevious
        view.match_next_recycler_view.adapter               = mAdapterNext
        mPresenterMatch                                     = PresenterMatch(this, ControllerApi())
        mPresenterMatch.loadPrevious(mLeagueId)
        mPresenterMatch.loadNext(mLeagueId)
        mPresenterMatch.loadLeague(mLeagueId)
    }

    private fun populateLeague(listLeague: MutableList<ModelLeague>){
        for (i in listLeague.indices){
            view?.match_desc_league?.text = listLeague.get(i).strDescriptionEN
            view?.context?.let { Glide.with(it).load(listLeague.get(i).strBadge).into(match_image_league) }
        }
    }
}
