package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFavoriteMatch
import com.muktiwibowo.footballapps.model.ModelFavorite
import com.muktiwibowo.footballapps.presenter.PresenterFavoriteMatch
import com.muktiwibowo.footballapps.view.ViewFavoriteMatch
import kotlinx.android.synthetic.main.fragment_favorite_match.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

class FragmentFavoriteMatch : Fragment(), ViewFavoriteMatch, AdapterFavoriteMatch.FavoriteListener{

    private var listFavorite: MutableList<ModelFavorite> = mutableListOf()
    private lateinit var mPresenterFavorite: PresenterFavoriteMatch
    private lateinit var mAdapterFavorite: AdapterFavoriteMatch

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite_match, container, false)
        initViews(view)
        return view
    }

    private fun initViews(view: View) {
        view.favorite_match_recycler_view.layoutManager = LinearLayoutManager(view.context)
        mAdapterFavorite = AdapterFavoriteMatch(listFavorite)
        view.favorite_match_recycler_view.adapter = mAdapterFavorite
        mPresenterFavorite = PresenterFavoriteMatch(view.context, this)
        mAdapterFavorite.setFavoriteLongListener(this)
        mPresenterFavorite.getFavoriteMatch()
    }

    override fun showFavoriteMatch(listFavoriteMatch: Collection<ModelFavorite>) {
        listFavorite.clear()
        listFavorite.addAll(listFavoriteMatch)
        mAdapterFavorite.notifyDataSetChanged()
    }

    override fun onFavoriteDeleted(position: Int) {
        val modelFavorite = mAdapterFavorite.listFavoriteMatch[position]
        context?.alert("Are you sure to delete this favorite ??") {
            title = "Delete Favorite"
            yesButton {
                mPresenterFavorite.deleteFavorite(modelFavorite.idFavorite)
                listFavorite.clear()
                mAdapterFavorite.notifyDataSetChanged()
                mPresenterFavorite.getFavoriteMatch()
            }
            noButton { }
        }?.show()
    }
}
