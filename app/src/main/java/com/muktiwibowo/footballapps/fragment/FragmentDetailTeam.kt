package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelDetailTeam
import com.muktiwibowo.footballapps.presenter.PresenterDetailTeam
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewDetailTeam
import kotlinx.android.synthetic.main.fragment_detail_team.*
import kotlinx.android.synthetic.main.fragment_detail_team.view.*
import android.database.sqlite.SQLiteConstraintException
import com.muktiwibowo.footballapps.controller.database
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.toast

class FragmentDetailTeam : Fragment(), ViewDetailTeam {

    private lateinit var mIdTeam: String
    private lateinit var mPresenterDetailTeam: PresenterDetailTeam
    private var mListDetailTeam: MutableList<ModelDetailTeam> = mutableListOf()

    fun newInstance(teamId: String?): FragmentDetailTeam{
        val fragmentDetailTeam  = FragmentDetailTeam()
        val bundle              = Bundle()
        bundle.putString(UtilConstant.FOOTBALL_ARGUMENT_ID_TEAM, teamId)
        fragmentDetailTeam.arguments = bundle
        return fragmentDetailTeam
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIdTeam = arguments?.getString(UtilConstant.FOOTBALL_ARGUMENT_ID_TEAM).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_team, container, false)
        view.detail_team_fab.setOnClickListener { saveFavoriteTeam()}
        mPresenterDetailTeam = PresenterDetailTeam(this, ControllerApi())
        mPresenterDetailTeam.loadDetailTeam(mIdTeam)
        return view
    }

    override fun showDetailTeam(listDetailTeam: Collection<ModelDetailTeam>) {
        mListDetailTeam.clear()
        mListDetailTeam.addAll(listDetailTeam)
        populateDetailTeam(mListDetailTeam)
    }

    private fun populateDetailTeam(listLeague: MutableList<ModelDetailTeam>){
        for (i in listLeague.indices){
            detail_team_name.text       = listLeague[i].strTeam
            detail_team_year.text       = listLeague[i].intFormedYear
            detail_team_desc.text       = listLeague[i].strDescriptionEN
            view?.context?.let { Glide.with(it).load(listLeague[i].strTeamBadge).into(detail_team_image) }
        }
    }

    private fun saveFavoriteTeam(){
        try {

            var teamId      = ""
            var teamName    = ""
            var teamYear    = ""
            var teamCountry = ""
            var teamStadium = ""
            var teamBadge   = ""

            for (i in mListDetailTeam.indices){
               teamId       = mListDetailTeam[i].idTeam.toString()
               teamName     = mListDetailTeam[i].strTeam.toString()
                teamYear    = mListDetailTeam[i].intFormedYear.toString()
                teamCountry = mListDetailTeam[i].strCountry.toString()
                teamStadium = mListDetailTeam[i].strStadium.toString()
                teamBadge   = mListDetailTeam[i].strTeamBadge.toString()
            }
            context?.database?.use {
                insert(
                    ModelFavoriteTeam.FAVORITE_TEAM,
                    ModelFavoriteTeam.TEAM_ID to teamId,
                    ModelFavoriteTeam.TEAM_NAME to teamName,
                    ModelFavoriteTeam.TEAM_YEAR to teamYear,
                    ModelFavoriteTeam.TEAM_COUNTRY to teamCountry,
                    ModelFavoriteTeam.TEAM_STADIUM to teamStadium,
                    ModelFavoriteTeam.TEAM_BADGE to teamBadge
                )
            }
            context?.toast("Successfully save to favorite")
        }catch (e: SQLiteConstraintException){
            context?.toast("Save failed")
            e.localizedMessage
        }
    }

}
