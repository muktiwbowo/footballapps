package com.muktiwibowo.footballapps.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFavoriteTeam
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam
import com.muktiwibowo.footballapps.presenter.PresenterFavoriteTeam
import com.muktiwibowo.footballapps.view.ViewFavoriteTeam
import kotlinx.android.synthetic.main.fragment_favorite_team.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

class FragmentFavoriteTeam : Fragment(), ViewFavoriteTeam, AdapterFavoriteTeam.FavoriteListener {

    private             var mListFavoriteTeam: MutableList<ModelFavoriteTeam> = mutableListOf()
    private lateinit    var mAdapterFavoriteTeam: AdapterFavoriteTeam
    private lateinit    var mPresenterFavoriteTeam: PresenterFavoriteTeam

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite_team, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        view.favorite_team_recycler_view.layoutManager  = LinearLayoutManager(context)
        mAdapterFavoriteTeam                            = AdapterFavoriteTeam(mListFavoriteTeam)
        view.favorite_team_recycler_view.adapter        = mAdapterFavoriteTeam
        mPresenterFavoriteTeam                          = PresenterFavoriteTeam(view.context, this)
        mAdapterFavoriteTeam.setFavoriteLongListener(this)
        mPresenterFavoriteTeam.getFavoriteTeam()
    }

    override fun showFavoriteTeam(listFavoriteTeam: Collection<ModelFavoriteTeam>) {
        mListFavoriteTeam.clear()
        mListFavoriteTeam.addAll(listFavoriteTeam)
        mAdapterFavoriteTeam.notifyDataSetChanged()
    }

    override fun onFavoriteDeleted(position: Int) {
        val modelFavorite = mAdapterFavoriteTeam.listFavoriteTeam[position]
        context?.alert("Are you sure to delete this favorite ??") {
            title = "Delete Favorite"
            yesButton {
                mPresenterFavoriteTeam.deleteFavorite(modelFavorite.idFavorite)
                mListFavoriteTeam.clear()
                mAdapterFavoriteTeam.notifyDataSetChanged()
                mPresenterFavoriteTeam.getFavoriteTeam()
            }
            noButton { }
        }?.show()
    }
}
