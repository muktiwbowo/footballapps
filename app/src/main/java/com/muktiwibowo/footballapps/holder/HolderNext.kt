package com.muktiwibowo.footballapps.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.muktiwibowo.footballapps.activity.ActivityDetail
import com.muktiwibowo.footballapps.model.ModelNext
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_next.view.*

class HolderNext(view: View): RecyclerView.ViewHolder(view) {
    fun bindNext(modelNext: ModelNext){
        itemView.next_date.text         = modelNext.dateEvent
        itemView.next_home_team.text    = modelNext.strHomeTeam
        itemView.next_away_team.text    = modelNext.strAwayTeam
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetail::class.java)
            intent.putExtra(UtilConstant.FOOTBALL_INTENT_DETAIL, modelNext.idEvent)
            itemView.context.startActivity(intent)
        }
    }
}