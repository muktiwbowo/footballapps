package com.muktiwibowo.footballapps.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.activity.ActivityDetailPlayer
import com.muktiwibowo.footballapps.model.ModelPlayer
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_player.view.*

class HolderPlayer(view: View): RecyclerView.ViewHolder(view) {
    fun bindPlayer(modelPlayer: ModelPlayer){
        itemView.player_name.text             = modelPlayer.strPlayer
        itemView.player_stadium.text          = modelPlayer.strTeam
        itemView.player_country.text          = modelPlayer.dateBorn
        itemView.player_year.text             = modelPlayer.strBirthLocation
        Glide.with(itemView.context).load(modelPlayer.strCutout).into(itemView.player_image)
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetailPlayer::class.java)
            intent.putExtra(UtilConstant.FOOTBALL_INTENT_TEAM_DETAIL, modelPlayer.idPlayer)
            itemView.context.startActivity(intent)
        }
    }
}