package com.muktiwibowo.footballapps.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.activity.ActivityTeam
import com.muktiwibowo.footballapps.model.ModelTeam
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_team.view.*

class HolderTeam(view: View): RecyclerView.ViewHolder(view) {
    fun bindTeam(modelTeam: ModelTeam){
        itemView.team_name.text             = modelTeam.strTeam
        itemView.team_stadium.text          = modelTeam.strStadium
        itemView.team_country.text          = modelTeam.strCountry
        itemView.team_year.text             = modelTeam.intFormedYear
        Glide.with(itemView.context).load(modelTeam.strTeamBadge).into(itemView.team_image)
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityTeam::class.java)
            intent.putExtra(UtilConstant.FOOTBALL_INTENT_TEAM_DETAIL, modelTeam.idTeam)
            itemView.context.startActivity(intent)
        }
    }
}