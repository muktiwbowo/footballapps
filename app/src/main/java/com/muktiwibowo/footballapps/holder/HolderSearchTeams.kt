package com.muktiwibowo.footballapps.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.activity.ActivitySearchTeam
import com.muktiwibowo.footballapps.model.ModelSearchTeam
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_search_team.view.*

class HolderSearchTeams(view: View): RecyclerView.ViewHolder(view) {
    fun bindSearchTeam(modelSearchTeam: ModelSearchTeam){
        itemView.search_team_name.text             = modelSearchTeam.strTeam
        itemView.search_team_stadium.text          = modelSearchTeam.strStadium
        itemView.search_team_country.text          = modelSearchTeam.strCountry
        itemView.search_team_year.text             = modelSearchTeam.intFormedYear
        Glide.with(itemView.context).load(modelSearchTeam.strTeamBadge).into(itemView.search_team_image)
    }
}