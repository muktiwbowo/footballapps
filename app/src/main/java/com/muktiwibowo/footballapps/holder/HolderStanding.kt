package com.muktiwibowo.footballapps.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.muktiwibowo.footballapps.model.ModelStanding
import kotlinx.android.synthetic.main.holder_standing.view.*

class HolderStanding(view: View): RecyclerView.ViewHolder(view) {
    fun bindStanding(modelStanding: ModelStanding){
        itemView.standing_name.text             = "Name: "+modelStanding.name
        itemView.standing_win.text              = "Win: "+modelStanding.win
        itemView.standing_loss.text             = "Loss: "+modelStanding.loss
        itemView.standing_draw.text             = "Draw: "+modelStanding.draw
        itemView.standing_played.text           = "Played: "+modelStanding.played
        itemView.standing_total.text            = "Total: "+modelStanding.total
        itemView.standing_goalsfor.text         = "GoalsFor: "+modelStanding.goalsfor
        itemView.standing_goalsagainst.text     = "Against: "+modelStanding.goalsagainst
        itemView.standing_goalsdifference.text  = "Difference: "+modelStanding.goalsdifference
    }
}