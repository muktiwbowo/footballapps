package com.muktiwibowo.footballapps.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.muktiwibowo.footballapps.adapter.AdapterFavoriteMatch
import com.muktiwibowo.footballapps.model.ModelFavorite
import kotlinx.android.synthetic.main.holder_favorite_match.view.*

class HolderFavoriteMatch(view: View): RecyclerView.ViewHolder(view) {
    fun bindFavorite(modelFavorite: ModelFavorite){
        itemView.view_favorite_name_left.text        = modelFavorite.dateEvent
        itemView.view_favorite_score_left.text   = modelFavorite.homeTeam
        itemView.view_favorite_date.text  = modelFavorite.homeScore
        itemView.view_favorite_name_right.text  = modelFavorite.awayTeam
        itemView.view_favorite_score_right.text = modelFavorite.awayScore
    }

    fun setFavoriteListener(favoriteListener: AdapterFavoriteMatch.FavoriteListener){
        itemView.setOnLongClickListener {
            favoriteListener.onFavoriteDeleted(adapterPosition)
            return@setOnLongClickListener true
        }
    }
}