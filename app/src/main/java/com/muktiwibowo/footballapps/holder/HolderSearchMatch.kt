package com.muktiwibowo.footballapps.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.muktiwibowo.footballapps.model.ModelSearchEvent
import kotlinx.android.synthetic.main.holder_search_match.view.*

class HolderSearchMatch(view: View): RecyclerView.ViewHolder(view) {
    fun bindSearchMatch(modelSearchMatch: ModelSearchEvent){
        itemView.search_match_name_left.text        = modelSearchMatch.dateEvent
        itemView.search_match_score_left.text   = modelSearchMatch.strHomeTeam
        itemView.search_match_date.text  = modelSearchMatch.intHomeScore
        itemView.search_match_name_right.text  = modelSearchMatch.strAwayTeam
        itemView.search_match_score_right.text = modelSearchMatch.intAwayScore
    }
}