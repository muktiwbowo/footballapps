package com.muktiwibowo.footballapps.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.adapter.AdapterFavoriteTeam
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam
import kotlinx.android.synthetic.main.holder_favorite_team.view.*

class HolderFavoriteTeam(view: View): RecyclerView.ViewHolder(view) {
    fun bindFavoriteTeam(modelFavoriteTeam: ModelFavoriteTeam){
        itemView.favorite_team_name.text             = modelFavoriteTeam.teamName
        itemView.favorite_team_stadium.text          = modelFavoriteTeam.teamStadium
        itemView.favorite_team_country.text          = modelFavoriteTeam.teamCountry
        itemView.favorite_team_year.text             = modelFavoriteTeam.teamYear
        Glide.with(itemView.context).load(modelFavoriteTeam.teamBadge).into(itemView.favorite_team_image)
    }

    fun setFavoriteListener(favoriteListener: AdapterFavoriteTeam.FavoriteListener){
        itemView.setOnLongClickListener {
            favoriteListener.onFavoriteDeleted(adapterPosition)
            return@setOnLongClickListener true
        }
    }
}