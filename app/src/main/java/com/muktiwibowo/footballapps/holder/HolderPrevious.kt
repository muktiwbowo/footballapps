package com.muktiwibowo.footballapps.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.muktiwibowo.footballapps.activity.ActivityDetail
import com.muktiwibowo.footballapps.model.ModelPrevious
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.holder_previous.view.*

class HolderPrevious(view: View): RecyclerView.ViewHolder(view) {
    fun bindMatch(modelPrevious: ModelPrevious){
        itemView.match_home_team.text       = modelPrevious.strHomeTeam
        itemView.match_home_score.text      = modelPrevious.intHomeScore
        itemView.match_away_team.text       = modelPrevious.strAwayTeam
        itemView.match_away_score.text      = modelPrevious.intAwayScore
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetail::class.java)
            intent.putExtra(UtilConstant.FOOTBALL_INTENT_DETAIL, modelPrevious.idEvent)
            itemView.context.startActivity(intent)
        }
    }
}