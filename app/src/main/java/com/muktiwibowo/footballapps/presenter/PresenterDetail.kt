package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListDetail
import com.muktiwibowo.footballapps.view.ViewDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterDetail(viewDetail: ViewDetail, controllerApi: ControllerApi) {
    private var mViewDetail: ViewDetail          = viewDetail
    private var mControllerApi: ControllerApi    = controllerApi

    fun getDetail(idEvent: String){
        mControllerApi.newInstance().getDetailMatch(idEvent).enqueue(object : Callback<ModelListDetail>{
            override fun onResponse(call: Call<ModelListDetail>, response: Response<ModelListDetail>) {
                response.body()?.listDetail?.let { mViewDetail.showDetail(it) }
            }

            override fun onFailure(call: Call<ModelListDetail>, t: Throwable) {

            }
        })
    }
}