package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListSearchTeam
import com.muktiwibowo.footballapps.view.ViewSearchTeam
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterSearchTeam(viewSearchTeam: ViewSearchTeam, controllerApi: ControllerApi) {
    private var mViewSearchTeam: ViewSearchTeam     = viewSearchTeam
    private var mControllerApi: ControllerApi       = controllerApi

    fun loadSearchTeam(query: String?){
        mControllerApi.newInstance().getSearchTeam(query).enqueue(object : Callback<ModelListSearchTeam> {
            override fun onResponse(call: Call<ModelListSearchTeam>, response: Response<ModelListSearchTeam>) {
                response.body()?.listTeams?.let { mViewSearchTeam.showSearchTeam(it) }
            }

            override fun onFailure(call: Call<ModelListSearchTeam>, t: Throwable) {

            }
        })
    }
}