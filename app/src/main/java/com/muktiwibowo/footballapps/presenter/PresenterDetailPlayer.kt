package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListDetailPlayer
import com.muktiwibowo.footballapps.view.ViewDetailPlayer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterDetailPlayer(viewDetailPlayer: ViewDetailPlayer, controllerApi: ControllerApi) {
    private var mViewDetailPlayer: ViewDetailPlayer = viewDetailPlayer
    private var mControllerApi: ControllerApi = controllerApi

    fun loadDetailPlayer(idPlayer: String){
        mControllerApi.newInstance().getPlayerDetail(idPlayer).enqueue(object : Callback<ModelListDetailPlayer>{
            override fun onResponse(call: Call<ModelListDetailPlayer>, response: Response<ModelListDetailPlayer>) {
                response.body()?.listDetailPlayers?.let { mViewDetailPlayer.showDetailPlayer(it) }
            }

            override fun onFailure(call: Call<ModelListDetailPlayer>, t: Throwable) {

            }
        })
    }
}