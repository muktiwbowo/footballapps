package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListPlayer
import com.muktiwibowo.footballapps.view.ViewPlayer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterPlayer(viewPlayer: ViewPlayer, controllerApi: ControllerApi) {
    private var mViewPlayer: ViewPlayer         = viewPlayer
    private var mControllerApi: ControllerApi   = controllerApi

    fun loadPlayer(teamId: String){
        mControllerApi.newInstance().getPlayer(teamId).enqueue(object : Callback<ModelListPlayer>{
            override fun onResponse(call: Call<ModelListPlayer>, response: Response<ModelListPlayer>) {
                response.body()?.listPlayers?.let { mViewPlayer.showPlayer(it) }
            }

            override fun onFailure(call: Call<ModelListPlayer>, t: Throwable) {

            }
        })
    }
}