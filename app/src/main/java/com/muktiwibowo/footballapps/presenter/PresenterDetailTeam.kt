package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListDetailTeam
import com.muktiwibowo.footballapps.view.ViewDetailTeam
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterDetailTeam(viewDetailTeam: ViewDetailTeam, controllerApi: ControllerApi) {
    private var mViewDetailTeam: ViewDetailTeam = viewDetailTeam
    private var mControllerApi: ControllerApi = controllerApi

    fun loadDetailTeam(teamId: String){
        mControllerApi.newInstance().getDetailTeam(teamId).enqueue(object : Callback<ModelListDetailTeam>{
            override fun onResponse(call: Call<ModelListDetailTeam>, response: Response<ModelListDetailTeam>) {
                response.body()?.listDetailTeams?.let { mViewDetailTeam.showDetailTeam(it) }
            }

            override fun onFailure(call: Call<ModelListDetailTeam>, t: Throwable) {

            }
        })
    }
}