package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListStanding
import com.muktiwibowo.footballapps.view.ViewStanding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterStanding(viewStanding: ViewStanding, controllerApi: ControllerApi) {
    private var mViewStanding: ViewStanding = viewStanding
    private var mControllerApi: ControllerApi = controllerApi

    fun loadStanding(leagueId: String){
        mControllerApi.newInstance().getStanding(leagueId).enqueue(object : Callback<ModelListStanding>{
            override fun onResponse(call: Call<ModelListStanding>, response: Response<ModelListStanding>) {
                response.body()?.listStandings?.let { mViewStanding.showStanding(it) }
            }

            override fun onFailure(call: Call<ModelListStanding>, t: Throwable) {

            }
        })
    }
}