package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListTeam
import com.muktiwibowo.footballapps.view.ViewTeam
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterTeam(viewTeam: ViewTeam, controllerApi: ControllerApi) {
    private var mViewTeam: ViewTeam = viewTeam
    private var mControllerApi: ControllerApi = controllerApi

    fun loadTeam(leagueId: String){
        mControllerApi.newInstance().getTeam(leagueId).enqueue(object : Callback<ModelListTeam>{
            override fun onResponse(call: Call<ModelListTeam>, response: Response<ModelListTeam>) {
                response.body()?.listTeams?.let { mViewTeam.showTeam(it) }
            }

            override fun onFailure(call: Call<ModelListTeam>, t: Throwable) {

            }
        })
    }
}