package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListLeague
import com.muktiwibowo.footballapps.model.listing.ModelListPrevious
import com.muktiwibowo.footballapps.model.listing.ModelListNext
import com.muktiwibowo.footballapps.view.ViewMatch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterMatch(viewMatch: ViewMatch, controllerApi: ControllerApi) {
    private var mViewMatch      : ViewMatch     = viewMatch
    private var mControllerApi  : ControllerApi = controllerApi

    fun loadPrevious(idLeague: String){
        mControllerApi.newInstance().getPrevMatch(idLeague).enqueue(object : Callback<ModelListPrevious>{
            override fun onResponse(call: Call<ModelListPrevious>, response: Response<ModelListPrevious>) {
                response.body()?.listPrevious?.let { mViewMatch.showPrevious(it) }
            }

            override fun onFailure(call: Call<ModelListPrevious>, t: Throwable) {

            }
        })
    }

    fun loadNext(idLeague: String){
        mControllerApi.newInstance().getNextMatch(idLeague).enqueue(object : Callback<ModelListNext>{
            override fun onResponse(call: Call<ModelListNext>, response: Response<ModelListNext>) {
                response.body()?.listNexts?.let { mViewMatch.showNext(it) }
            }

            override fun onFailure(call: Call<ModelListNext>, t: Throwable) {

            }
        })
    }

    fun loadLeague(idLeague: String){
        mControllerApi.newInstance().getLeagueDetail(idLeague).enqueue(object : Callback<ModelListLeague>{
            override fun onResponse(call: Call<ModelListLeague>, response: Response<ModelListLeague>) {
                response.body()?.listLeagues?.let { mViewMatch.showLeague(it) }
            }

            override fun onFailure(call: Call<ModelListLeague>, t: Throwable) {

            }
        })
    }
}