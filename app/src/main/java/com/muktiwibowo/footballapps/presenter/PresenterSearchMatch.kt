package com.muktiwibowo.footballapps.presenter

import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.listing.ModelListSearchEvent
import com.muktiwibowo.footballapps.view.ViewSearchMatch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterSearchMatch(viewSearchMatch: ViewSearchMatch, controllerApi: ControllerApi) {
    private var mViewSearchMatch: ViewSearchMatch   = viewSearchMatch
    private var mControllerApi: ControllerApi       = controllerApi

    fun loadSearchMatch(query: String?){
        mControllerApi.newInstance().getSearchMatch(query).enqueue(object : Callback<ModelListSearchEvent>{
            override fun onResponse(call: Call<ModelListSearchEvent>, response: Response<ModelListSearchEvent>) {
                response.body()?.listSearchEvents?.let { mViewSearchMatch.showSearchMatch(it) }
            }

            override fun onFailure(call: Call<ModelListSearchEvent>, t: Throwable) {

            }
        })
    }
}