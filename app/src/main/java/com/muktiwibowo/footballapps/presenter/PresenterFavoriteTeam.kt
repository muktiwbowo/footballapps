package com.muktiwibowo.footballapps.presenter

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.muktiwibowo.footballapps.controller.database
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam
import com.muktiwibowo.footballapps.view.ViewFavoriteTeam
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.delete

class PresenterFavoriteTeam(val context: Context, val viewFavoriteTeam: ViewFavoriteTeam) {
    fun getFavoriteTeam(){
        try {
            context.database.use {
                val listFavoriteTeam    = select(ModelFavoriteTeam.FAVORITE_TEAM)
                val favoriteTeam        = listFavoriteTeam.parseList(classParser<ModelFavoriteTeam>())
                Log.e("PresenterFavorite", ""+favoriteTeam)
                favoriteTeam.let {
                    viewFavoriteTeam.showFavoriteTeam(it)
                }
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }

    fun deleteFavorite(id: Int){
        try {
            context.database.use {
                delete(ModelFavoriteTeam.FAVORITE_TEAM,
                    "id_ =  {idFavoriteTeam}",
                    "idFavoriteTeam" to id)
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }
}