package com.muktiwibowo.footballapps.presenter

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.muktiwibowo.footballapps.controller.database
import com.muktiwibowo.footballapps.model.ModelFavorite
import com.muktiwibowo.footballapps.view.ViewFavoriteMatch
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.select

class PresenterFavoriteMatch (val context: Context, val viewFavoriteMatch: ViewFavoriteMatch) {
    fun getFavoriteMatch(){
        try {
            context.database.use {
                val listFavorite    = select(ModelFavorite.TABLE_FAVORITE)
                val favorite        = listFavorite.parseList(classParser<ModelFavorite>())
                favorite.let {
                    viewFavoriteMatch.showFavoriteMatch(it)
                }
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }

    fun deleteFavorite(id: Int){
        try {
            context.database.use {
                delete(ModelFavorite.TABLE_FAVORITE,
                    "id_ =  {idFavorite}",
                    "idFavorite" to id)
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }
}