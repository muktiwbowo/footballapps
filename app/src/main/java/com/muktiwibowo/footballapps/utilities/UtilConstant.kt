package com.muktiwibowo.footballapps.utilities

object UtilConstant {
    const val FOOTBALL_INTENT_ID_LEAGUE         = "league"
    const val FOOTBALL_ARGUMENT_ID_LEAGUE       = "league_id"
    const val FOOTBALL_ARGUMENT_ID_TEAM         = "team_id"
    const val FOOTBALL_TITLE_FRAGMENT_MATCH     = "Match"
    const val FOOTBALL_TITLE_FRAGMENT_STANDING  = "Standing"
    const val FOOTBALL_TITLE_FRAGMENT_TEAM      = "Team"
    const val FOOTBALL_TITLE_FAVORITE_MATCH     = "Favorite Match"
    const val FOOTBALL_TITLE_FAVORITE_TEAM      = "Favorite Team"
    const val FOOTBALL_TITLE_DETAIL_TEAM        = "Detail Team"
    const val FOOTBALL_TITLE_PLAYER             = "Player"
    const val FOOTBALL_INTENT_DETAIL            = "detail_match"
    const val FOOTBALL_INTENT_TEAM_DETAIL       = "detail_team"
}