package com.muktiwibowo.footballapps.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.controller.database
import com.muktiwibowo.footballapps.model.ModelDetail
import com.muktiwibowo.footballapps.model.ModelFavorite
import com.muktiwibowo.footballapps.presenter.PresenterDetail
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.utilities.UtilString
import com.muktiwibowo.footballapps.view.ViewDetail
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.toast

class ActivityDetail : AppCompatActivity(), ViewDetail {

    private lateinit var mPresenterDetail: PresenterDetail
    private var mListDetail: MutableList<ModelDetail> = mutableListOf()
    private lateinit var mIdEvent: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        getPassingIntent(intent)
        mPresenterDetail = PresenterDetail(this, ControllerApi())
        mPresenterDetail.getDetail(mIdEvent)
        initToolbar()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.football_save -> saveFavorite()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar() {
        setSupportActionBar(detail_toolbar)
        supportActionBar?.title = "Detail Match"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun getPassingIntent(intent: Intent) {
        mIdEvent = intent.getStringExtra(UtilConstant.FOOTBALL_INTENT_DETAIL)
    }

    private fun saveFavorite() {
        var idEvent = ""
        var homeTeam = ""
        var homeScore = ""
        var awayTeam = ""
        var awayScore = ""
        var dateEvent = ""
        for (i in mListDetail.indices) {
            idEvent = mListDetail[i].idEvent.toString()
            homeTeam = mListDetail[i].strHomeTeam.toString()
            homeScore = mListDetail[i].intHomeScore.toString()
            awayTeam = mListDetail[i].strAwayTeam.toString()
            awayScore = mListDetail[i].intAwayScore.toString()
            dateEvent = mListDetail[i].dateEvent.toString()

        }
        try {
            database.use {
                insert(
                    ModelFavorite.TABLE_FAVORITE,
                    ModelFavorite.ID_EVENT to idEvent,
                    ModelFavorite.HOME_NAME to homeTeam,
                    ModelFavorite.HOME_SCORE to homeScore,
                    ModelFavorite.AWAY_NAME to awayTeam,
                    ModelFavorite.AWAY_SCORE to awayScore,
                    ModelFavorite.DATE_EVENT to dateEvent
                )
            }
            toast("successfully inserted")
        } catch (e: SQLiteConstraintException) {
            toast("failed to insert favorit")
            e.localizedMessage
        }
    }


    override fun showDetail(listDetail: Collection<ModelDetail>) {
        mListDetail.clear()
        mListDetail.addAll(listDetail)
        populateView(mListDetail)
    }

    @SuppressLint("SetTextI18n")
    private fun populateView(mEvent: List<ModelDetail>) {
        for (i in mEvent.indices) {
            view_detail_date.text = "Date: " + UtilString.checkString(mEvent.get(i).dateEvent)
            view_detail_name_left.text = UtilString.checkString(mEvent.get(i).strHomeTeam)
            view_detail_score_left.text = UtilString.checkString(mEvent.get(i).intHomeScore)
            view_left_goal_detail.text = "Goal Detail: " + UtilString.checkString(mEvent.get(i).strHomeGoalDetails)
            view_left_redcard.text = "Red Card: " + UtilString.checkString(mEvent.get(i).strHomeRedCards)
            view_left_yellowcard.text = "Yellow Card: " + UtilString.checkString(mEvent.get(i).strHomeYellowCards)
            view_left_lineup_defense.text = "Defense: " + UtilString.checkString(mEvent.get(i).strHomeLineupDefense)
            view_left_lineup_goalkeeper.text =
                "Goal Keeper: " + UtilString.checkString(mEvent.get(i).strHomeLineupGoalkeeper)
            view_left_lineup_midfield.text = "Mid Field: " + UtilString.checkString(mEvent.get(i).strHomeLineupMidfield)
            view_left_lineup_forward.text = "Forward: " + UtilString.checkString(mEvent.get(i).strHomeLineupForward)
            view_left_subtitutes.text = "Subtitutes: " + UtilString.checkString(mEvent.get(i).strHomeLineupSubstitutes)
            view_detail_name_right.text = UtilString.checkString(mEvent.get(i).strAwayTeam)
            view_detail_score_right.text = UtilString.checkString(mEvent.get(i).intAwayScore)
            view_right_goal_detail.text = "Goal Detail: " + UtilString.checkString(mEvent.get(i).strAwayGoalDetails)
            view_right_redcard.text = "Red Card: " + UtilString.checkString(mEvent.get(i).strAwayRedCards)
            view_right_yellowcard.text = "Yellow Card: " + UtilString.checkString(mEvent.get(i).strAwayYellowCards)
            view_right_lineup_defense.text = "Defense: " + UtilString.checkString(mEvent.get(i).strAwayLineupDefense)
            view_right_lineup_goalkeeper.text =
                "Goal Keeper: " + UtilString.checkString(mEvent.get(i).strAwayLineupGoalkeeper)
            view_right_lineup_midfield.text =
                "Mid Field: " + UtilString.checkString(mEvent.get(i).strAwayLineupMidfield)
            view_right_lineup_forward.text = "Forward: " + UtilString.checkString(mEvent.get(i).strAwayLineupForward)
            view_right_subtitutes.text = "Subtitutes: " + UtilString.checkString(mEvent.get(i).strAwayLineupSubstitutes)
            if (mEvent.get(i).strThumb.isNullOrEmpty()) {
                view_detail_placeholder.visibility = View.VISIBLE
            } else {
                Glide.with(this).load(mEvent.get(i).strThumb)
                    .centerCrop()
                    .into(view_detail_image.findViewById(R.id.view_detail_image))
            }
        }
    }
}
