package com.muktiwibowo.footballapps.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFragment
import com.muktiwibowo.footballapps.fragment.FragmentFavoriteMatch
import com.muktiwibowo.footballapps.fragment.FragmentFavoriteTeam
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.activity_favorite.*

class ActivityFavorite : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        initToolbar()
        initTabLayout()
    }

    private fun initToolbar(){
        setSupportActionBar(favorite_toolbar)
        supportActionBar?.title     = "Favorite"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initTabLayout(){
        val viewPagerAdapter = AdapterFragment(supportFragmentManager)
        viewPagerAdapter.addFragment(FragmentFavoriteMatch(), UtilConstant.FOOTBALL_TITLE_FAVORITE_MATCH)
        viewPagerAdapter.addFragment(FragmentFavoriteTeam(), UtilConstant.FOOTBALL_TITLE_FAVORITE_TEAM)
        favorite_viewpager.adapter = viewPagerAdapter
        favorite_tab.setupWithViewPager(favorite_viewpager)
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
