package com.muktiwibowo.footballapps.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterSearchMatch
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelSearchEvent
import com.muktiwibowo.footballapps.presenter.PresenterSearchMatch
import com.muktiwibowo.footballapps.view.ViewSearchMatch
import kotlinx.android.synthetic.main.activity_search_match.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.toast


class ActivitySearchMatch : AppCompatActivity(), ViewSearchMatch {

    private             var mListMatch: MutableList<ModelSearchEvent> = mutableListOf()
    private lateinit    var mAdapterSearchMatch: AdapterSearchMatch
    private lateinit    var mPresenterSearchMatch: PresenterSearchMatch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)
        initView()
    }

    private fun initView() {
        setSupportActionBar(search_match_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        search_match_recycle_view.layoutManager     = LinearLayoutManager(ctx)
        mAdapterSearchMatch                         = AdapterSearchMatch(mListMatch)
        search_match_recycle_view.adapter           = mAdapterSearchMatch
        mPresenterSearchMatch                       = PresenterSearchMatch(this, ControllerApi())
        search_match.requestFocus()
        search_match.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                mPresenterSearchMatch.loadSearchMatch(query)
                return true
            }

        })
    }

    override fun showSearchMatch(listMatch: Collection<ModelSearchEvent>) {
        mListMatch.clear()
        mListMatch.addAll(listMatch)
        mAdapterSearchMatch.notifyDataSetChanged()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
