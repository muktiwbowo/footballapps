package com.muktiwibowo.footballapps.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFragment
import com.muktiwibowo.footballapps.fragment.FragmentMatch
import com.muktiwibowo.footballapps.fragment.FragmentStanding
import com.muktiwibowo.footballapps.fragment.FragmentTeam
import com.muktiwibowo.footballapps.model.ModelFootball
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.activity_content.*

class ActivityContent : AppCompatActivity() {

    private lateinit var modelFootball: ModelFootball

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        getPassingIntent(intent)
        initToolbar()
        initTabLayout()
    }

    private fun initToolbar(){
        setSupportActionBar(content_toolbar)
        supportActionBar?.title     = "Detail League"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun getPassingIntent(intent: Intent){
        modelFootball = intent.getParcelableExtra(UtilConstant.FOOTBALL_INTENT_ID_LEAGUE)
        Log.e("ActivityContent", ""+modelFootball.footballId)
    }

    private fun initTabLayout(){
        val viewPagerAdapter = AdapterFragment(supportFragmentManager)
        viewPagerAdapter.addFragment(FragmentMatch().newInstance(modelFootball.footballId), UtilConstant.FOOTBALL_TITLE_FRAGMENT_MATCH)
        viewPagerAdapter.addFragment(FragmentStanding().newInstance(modelFootball.footballId), UtilConstant.FOOTBALL_TITLE_FRAGMENT_STANDING)
        viewPagerAdapter.addFragment(FragmentTeam().newInstance(modelFootball.footballId), UtilConstant.FOOTBALL_TITLE_FRAGMENT_TEAM)
        content_viewpager.adapter = viewPagerAdapter
        content_tab.setupWithViewPager(content_viewpager)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
