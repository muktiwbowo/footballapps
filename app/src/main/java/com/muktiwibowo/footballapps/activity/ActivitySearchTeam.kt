package com.muktiwibowo.footballapps.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterSearchTeam
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelSearchTeam
import com.muktiwibowo.footballapps.presenter.PresenterSearchTeam
import com.muktiwibowo.footballapps.view.ViewSearchTeam
import kotlinx.android.synthetic.main.activity_search_team.*
import org.jetbrains.anko.ctx

class ActivitySearchTeam : AppCompatActivity(), ViewSearchTeam {

    private             var mListTeam: MutableList<ModelSearchTeam> = mutableListOf()
    private lateinit    var mAdapterSearchTeam: AdapterSearchTeam
    private lateinit    var mPresenterSearchTeam: PresenterSearchTeam

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_team)
        initView()
    }

    private fun initView() {
        setSupportActionBar(search_team_toolbar)
        supportActionBar?.title     = "Search Team"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        search_team_recycle_view.layoutManager  = LinearLayoutManager(ctx)
        mAdapterSearchTeam                      = AdapterSearchTeam(mListTeam)
        search_team_recycle_view.adapter        = mAdapterSearchTeam
        mPresenterSearchTeam                    = PresenterSearchTeam(this, ControllerApi())
        search_team.requestFocus()
        search_team.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                mPresenterSearchTeam.loadSearchTeam(query)
                return true
            }

        })
    }

    override fun showSearchTeam(listTeam: Collection<ModelSearchTeam>) {
        mListTeam.clear()
        mListTeam.addAll(listTeam)
        mAdapterSearchTeam.notifyDataSetChanged()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
