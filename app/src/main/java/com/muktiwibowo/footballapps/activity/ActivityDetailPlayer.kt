package com.muktiwibowo.footballapps.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.controller.ControllerApi
import com.muktiwibowo.footballapps.model.ModelDetailPlayer
import com.muktiwibowo.footballapps.presenter.PresenterDetailPlayer
import com.muktiwibowo.footballapps.utilities.UtilConstant
import com.muktiwibowo.footballapps.view.ViewDetailPlayer
import kotlinx.android.synthetic.main.activity_detail_player.*

class ActivityDetailPlayer : AppCompatActivity(), ViewDetailPlayer {

    private lateinit    var mIdPlayer: String
    private             var mListPlayers: MutableList<ModelDetailPlayer> = mutableListOf()
    private lateinit    var mPreseDetailPlayer: PresenterDetailPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)
        mIdPlayer   = intent.getStringExtra(UtilConstant.FOOTBALL_INTENT_TEAM_DETAIL)
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun initView(){
        setSupportActionBar(player_detail_toolbar)
        supportActionBar?.title     = "Detail Player"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mPreseDetailPlayer          = PresenterDetailPlayer(this, ControllerApi())
        mPreseDetailPlayer.loadDetailPlayer(mIdPlayer)
    }

    override fun showDetailPlayer(listDetailPlayer: Collection<ModelDetailPlayer>) {
        mListPlayers.clear()
        mListPlayers.addAll(listDetailPlayer)
        populateView(mListPlayers)
    }

    private fun populateView(listPlayers: MutableList<ModelDetailPlayer>) {
        for (i in listPlayers.indices){
            detail_player_name.text       = listPlayers[i].strTeam
            detail_player_year.text       = listPlayers[i].dateBorn
            detail_player_desc.text       = listPlayers[i].strDescriptionEN
            Glide.with(this).load(listPlayers[i].strCutout).into(detail_player_image)
        }
    }
}
