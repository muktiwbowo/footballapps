package com.muktiwibowo.footballapps.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFootball
import com.muktiwibowo.footballapps.model.ModelFootball
import kotlinx.android.synthetic.main.activity_football.*

class ActivityFootball : AppCompatActivity() {

    private var listFootballs: ArrayList<ModelFootball> = arrayListOf()
    private lateinit var mAdapterFootball: AdapterFootball

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_football)
        initToolbar()
        val footballId      = resources.getStringArray(R.array.league_id)
        val footballName    = resources.getStringArray(R.array.league_name)
        val footballImage   = resources.obtainTypedArray(R.array.league_image)

        for (i in 0..5){
            val modelFootball = ModelFootball(footballId[i], footballName[i], footballImage.getResourceId(i, 0))
            listFootballs.add(modelFootball)
        }
        mAdapterFootball = AdapterFootball(this, listFootballs)
        football_grid_view.adapter = mAdapterFootball
    }

    private fun initToolbar(){
        setSupportActionBar(football_toolbar)
        supportActionBar?.title = "Football Apps"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_football, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.football_search_match  -> startActivity(Intent(this, ActivitySearchMatch::class.java))
            R.id.football_search_team   -> startActivity(Intent(this, ActivitySearchTeam::class.java))
            R.id.football_favorite      -> startActivity(Intent(this, ActivityFavorite::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
