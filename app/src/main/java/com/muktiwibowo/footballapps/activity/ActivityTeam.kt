package com.muktiwibowo.footballapps.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.muktiwibowo.footballapps.R
import com.muktiwibowo.footballapps.adapter.AdapterFragment
import com.muktiwibowo.footballapps.fragment.FragmentDetailTeam
import com.muktiwibowo.footballapps.fragment.FragmentPlayer
import com.muktiwibowo.footballapps.utilities.UtilConstant
import kotlinx.android.synthetic.main.activity_team.*

class ActivityTeam : AppCompatActivity() {

    private lateinit var mIdTeam: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)
        getPassingIntent(intent)
        initToolbar()
        initTabLayout()
    }

    private fun getPassingIntent(intent: Intent){
        mIdTeam = intent.getStringExtra(UtilConstant.FOOTBALL_INTENT_TEAM_DETAIL)
    }

    private fun initToolbar(){
        setSupportActionBar(team_toolbar)
        supportActionBar?.title     = "Team"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initTabLayout(){
        val viewPagerAdapter = AdapterFragment(supportFragmentManager)
        viewPagerAdapter.addFragment(FragmentDetailTeam().newInstance(mIdTeam), UtilConstant.FOOTBALL_TITLE_DETAIL_TEAM)
        viewPagerAdapter.addFragment(FragmentPlayer().newInstance(mIdTeam), UtilConstant.FOOTBALL_TITLE_PLAYER)
        team_viewpager.adapter = viewPagerAdapter
        team_tab.setupWithViewPager(team_viewpager)
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
