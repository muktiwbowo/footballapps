package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelStanding

interface ViewStanding {
    fun showStanding(listStanding: Collection<ModelStanding>)
}