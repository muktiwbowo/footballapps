package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelSearchEvent

interface ViewSearchMatch {
    fun showSearchMatch(listMatch: Collection<ModelSearchEvent>)
}