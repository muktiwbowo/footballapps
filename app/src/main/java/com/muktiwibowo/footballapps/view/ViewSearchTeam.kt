package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelSearchTeam

interface ViewSearchTeam {
    fun showSearchTeam(listTeam: Collection<ModelSearchTeam>)
}