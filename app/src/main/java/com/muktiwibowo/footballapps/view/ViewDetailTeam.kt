package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelDetailTeam

interface ViewDetailTeam {
    fun showDetailTeam(listDetailTeam: Collection<ModelDetailTeam>)
}