package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelFavorite

interface ViewFavoriteMatch {
    fun showFavoriteMatch(listFavoriteMatch: Collection<ModelFavorite>)
}