package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelDetail
import com.muktiwibowo.footballapps.model.ModelNext
import com.muktiwibowo.footballapps.model.ModelPrevious

interface ViewDetail {
    fun showDetail(listDetail: Collection<ModelDetail>)
}