package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelFavoriteTeam

interface ViewFavoriteTeam {
    fun showFavoriteTeam(listFavoriteTeam: Collection<ModelFavoriteTeam>)
}