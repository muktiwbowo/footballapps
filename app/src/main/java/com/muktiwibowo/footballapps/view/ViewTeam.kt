package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelTeam

interface ViewTeam {
    fun showTeam(listTeam: Collection<ModelTeam>)
}