package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelLeague
import com.muktiwibowo.footballapps.model.ModelPrevious
import com.muktiwibowo.footballapps.model.ModelNext
import com.muktiwibowo.footballapps.model.listing.ModelListLeague

interface ViewMatch {
    fun showPrevious(listPrevious: Collection<ModelPrevious>)
    fun showNext(listNext: Collection<ModelNext>)
    fun showLeague(listLeague: Collection<ModelLeague>)
}