package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelDetailPlayer

interface ViewDetailPlayer {
    fun showDetailPlayer(listDetailPlayer: Collection<ModelDetailPlayer>)
}