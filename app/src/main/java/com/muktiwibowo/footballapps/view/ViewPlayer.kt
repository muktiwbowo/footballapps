package com.muktiwibowo.footballapps.view

import com.muktiwibowo.footballapps.model.ModelPlayer

interface ViewPlayer {
    fun showPlayer(listPlayer: Collection<ModelPlayer>)
}