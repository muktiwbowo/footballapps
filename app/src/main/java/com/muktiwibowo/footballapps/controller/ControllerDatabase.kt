package com.muktiwibowo.footballapps.controller

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.muktiwibowo.footballapps.model.ModelFavorite
import com.muktiwibowo.footballapps.model.ModelFavoriteTeam
import org.jetbrains.anko.db.*

class ControllerDatabase(context: Context):
        ManagedSQLiteOpenHelper(context, "favorite_db", null, 1) {
    companion object {
        private var mControllerDatabase: ControllerDatabase? = null

        @Synchronized
        fun getInstance(context: Context): ControllerDatabase{
            if (mControllerDatabase == null){
                mControllerDatabase = ControllerDatabase(context.applicationContext)
            }
            return mControllerDatabase!!
        }
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            ModelFavorite.TABLE_FAVORITE, true,
                ModelFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ModelFavorite.ID_EVENT to TEXT + UNIQUE,
                ModelFavorite.HOME_NAME to TEXT,
                ModelFavorite.HOME_SCORE to TEXT,
                ModelFavorite.DATE_EVENT to TEXT,
                ModelFavorite.AWAY_NAME to TEXT,
                ModelFavorite.AWAY_SCORE to TEXT)
        db?.createTable(
            ModelFavoriteTeam.FAVORITE_TEAM, true,
            ModelFavoriteTeam.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            ModelFavoriteTeam.TEAM_ID to TEXT + UNIQUE,
            ModelFavoriteTeam.TEAM_NAME to TEXT,
            ModelFavoriteTeam.TEAM_YEAR to TEXT,
            ModelFavoriteTeam.TEAM_COUNTRY to TEXT,
            ModelFavoriteTeam.TEAM_STADIUM to TEXT,
            ModelFavoriteTeam.TEAM_BADGE to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ModelFavorite.TABLE_FAVORITE, true)
        db?.dropTable(ModelFavoriteTeam.FAVORITE_TEAM, true)
    }
}
val Context.database: ControllerDatabase
    get() = ControllerDatabase.getInstance(applicationContext)