package com.muktiwibowo.footballapps.controller

import com.muktiwibowo.footballapps.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ControllerApi {
    fun newInstance(): ControllerRequest{
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ControllerRequest::class.java)
    }
}