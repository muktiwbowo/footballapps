package com.muktiwibowo.footballapps.controller

import com.muktiwibowo.footballapps.model.ModelSearchTeam
import com.muktiwibowo.footballapps.model.listing.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ControllerRequest {
    @GET("eventspastleague.php?")
    fun getPrevMatch(@Query("id") id: String?): Call<ModelListPrevious>

    @GET("eventsnextleague.php?")
    fun getNextMatch(@Query("id") id: String?): Call<ModelListNext>

    @GET("lookupleague.php?")
    fun getLeagueDetail(@Query("id") leagueId: String?): Call<ModelListLeague>

    @GET("lookupevent.php?")
    fun getDetailMatch(@Query("id") eventId: String): Call<ModelListDetail>

    @GET("lookuptable.php")
    fun getStanding(@Query("l") leagueId: String?): Call<ModelListStanding>

    @GET("lookup_all_teams.php")
    fun getTeam(@Query("id") leagueId: String?): Call<ModelListTeam>

    @GET("lookupteam.php")
    fun getDetailTeam(@Query("id") teamId: String?): Call<ModelListDetailTeam>

    @GET("lookup_all_players.php")
    fun getPlayer(@Query("id") teamId: String?): Call<ModelListPlayer>

    @GET("lookupplayer.php")
    fun getPlayerDetail(@Query("id") playerId: String?): Call<ModelListDetailPlayer>

    @GET("searchevents.php?")
    fun getSearchMatch(@Query("e") query: String?): Call<ModelListSearchEvent>

    @GET("searchteams.php")
    fun getSearchTeam(@Query("t") query: String?): Call<ModelListSearchTeam>
}